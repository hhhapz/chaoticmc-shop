package com.chaoticmc.shop

import com.chaoticmc.shop.api.event.PurchaseEvent
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import java.math.RoundingMode
import java.text.DecimalFormat

internal object InventoryManager {
  
  lateinit var config: FileConfiguration
  private val df = DecimalFormat("#.##")
  
  fun addInventories() {
    
    Shop.shop["main"] = makeInventory(config.getInt("settings.main.size"), config.string("settings.main.title"))
    
    for (it: String in config.getConfigurationSection("settings").getKeys(false)) {
      if (it == "main") continue
      
      Shop.shop[it] = com.chaoticmc.shop.makeInventory(getSize(it), config.string("settings.$it.title"))
      
    }
    
    df.roundingMode = RoundingMode.HALF_UP
  }
  
  
  private fun getSize(path: String): Int {
    var max = 0
    
    if (!Shop.shopConfig.isConfigurationSection(path)) max = 1
    else Shop.shopConfig.getConfigurationSection(path).getKeys(false).forEach { if (it.toInt() > max) max = it.toInt() }
    
    return (max / 9 + 1) * 9
  }
  
  fun addItems() {
    for (store in Shop.shop.keys) {
      if (store == "main") continue
      var prefix = "settings.$store"
      val shop = Shop.shop[store]!!
      
      Shop.shop["main"]!!.setItem(
        config.getInt("settings.$store.slot"), itemBuilder {
        item = Material.valueOf(config.string("$prefix.block", false))
        
        dispName = config.string("$prefix.title")
        
        lore = config.getStringList("$prefix.description")
      })
      
      
      if (Shop.shopConfig.isConfigurationSection(store))
        
        Shop.shopConfig.getConfigurationSection(store).getKeys(false).forEach {
          
          prefix = "$store.$it"
          
          
          shop.setItem(
            it.toInt(),
            
            
            itemBuilder {
              item = Material.valueOf(Shop.shopConfig.string("$prefix.material"))
              
              durability = if (Shop.shopConfig.contains("$prefix.durability"))
                Shop.shopConfig.getInt("$prefix.durability") else 0
              
              dispName =
                if (Shop.shopConfig.contains("$prefix.name")) Shop.shopConfig.string("$prefix.name")
                else "${ChatColor.YELLOW}${item.name}"
              
              val buy = Shop.shopConfig.getString("$prefix.buy")?.toFloat() ?: 0.0f
              val sell = Shop.shopConfig.getString("$prefix.sell")?.toFloat() ?: 0.0f
  
              if (buy != 0.0f) lore.add("&6&l• &eBuy &f$buy/ea &7(Left Click)".colors())
              if (sell != 0.0f) lore.add("&6&l• &eSell &f$buy/ea &7(Right Click)".colors())
            }
          )
        }
    }
  }
  
  fun buyOrSell(purchase: Purchase, amount: Int) {
    var transactionAmount = amount
    val durability = purchase.durability.toInt()
    
    with(purchase) {
      if (purchase.type == PurchaseType.BUY) {
        
        if (Shop.econ?.withdrawPlayer(player, price * amount.toDouble())?.transactionSuccess() == true) {
          
          player.inventory.addItem(makeBlock(material, amount, durability))
          player.sendMessage("&aYou have purchased $amount $material for ${price * amount}".colors())
          transaction = Transaction.SUCCESS
          
        } else player.sendMessage("&cYou don't have enough money!".colors())
        
      } else if (purchase.type == PurchaseType.SELL) when {
        
        inventoryContains(player.inventory, makeBlock(material, amount, durability)) -> {
          
          removeFromInventory(player.inventory, makeBlock(material, amount, durability), amount)
          Shop.econ?.depositPlayer(player, amount.toDouble() * price)
          
          player.sendMessage("&aYou have sold $amount $material for ${price * amount}".colors())
          
          transaction = Transaction.SUCCESS
          
        }
        
        getAllInventoryContains(player.inventory, makeBlock(material, 1, durability)) >= 1 -> {
          
          val all = getAllInventoryContains(player.inventory, makeBlock(material, 1, durability))
          
          removeFromInventory(player.inventory, makeBlock(material, all, durability), amount)
          Shop.econ?.depositPlayer(player, all.toDouble() * price)
          
          player.sendMessage("&aYou have sold $all $material for ${price * all}".colors())
          
          transactionAmount = all
          transaction = Transaction.SUCCESS
          
        }
        
        else -> player.sendMessage("&aYou don't have any items to sell!".colors())
      }
      
      
      Bukkit.getPluginManager().callEvent(PurchaseEvent(
        player,
        type,
        transaction,
        inventory,
        price,
        material,
        transactionAmount,
        durability
      ))
    }
  }
  
  fun makeInventory(inv: Inventory, currentItem: ItemStack, choice: String, price: Float) {
    for (i in 0..8) {
      var o = i
      if (i == 0) o = 1
      if (i == 1) o = 2
      if (i == 2) o = 4
      if (i == 3) o = 8
      if (i == 4) o = 16
      if (i == 5) o = 32
      if (i == 6) o = 32
      if (i == 7) o = 48
      if (i == 8) o = 64
      
      inv.setItem(
        i + 9,
        makeBlock(currentItem.type, o,
                  currentItem.durability.toInt(),
                  "&e$choice for ${df.format(price * o)}",
                  mutableListOf("&eClick to ${choice.toLowerCase()}.")))
      
      inv.setItem(i + 18, makeBlock(Material.STAINED_GLASS_PANE, 1, 7, "&2"))
      inv.setItem(i, makeBlock(Material.STAINED_GLASS_PANE, 1, 7, "&2"))
    }
    
    inv.setItem(26, makeBlock(Material.ARROW, dispName = "&cGo back"))
  }
  
}
