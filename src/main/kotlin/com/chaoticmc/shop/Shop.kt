package com.chaoticmc.shop

import com.chaoticmc.shop.InventoryManager.addInventories
import com.chaoticmc.shop.InventoryManager.addItems
import net.milkbowl.vault.economy.Economy
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.java.JavaPlugin
import kotlin.system.measureNanoTime


class Shop : JavaPlugin() {
  companion object {
    internal lateinit var instance: Shop
    internal lateinit var shopConfig: YamlConfiguration
    internal var shop = hashMapOf<String, Inventory>()
    internal var econ: Economy? = null
  }
  
  override fun onEnable() {
    instance = this
    saveDefaultConfig()
    setup()
    if (!setupEconomy()) {
      print(String.format("Shop - Disabled due to no Vault dependency found!"))
//      server.pluginManager.disablePlugin(this)
      return
    }
  }
  
  override fun onDisable() {
    saveResource("config.yml", false)
  }
  
  private fun setup() {
    shopConfig = ShopConfig.makeFiles()
    InventoryManager.config = config
    addInventories()
    addItems()
    server.pluginManager.registerEvents(ClickListener(), this)
  }
  
  override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
    if (command.name.equals("shop", false)) {
      if (args.size == 1 && args.component1() == "reload" && sender.hasPermission("shop.admin")) {
        sender.sendMessage("&aConfiguration reloaded in ${measureNanoTime { reloadAll() }} nanoseconds.".colors())
      } else (sender as? Player)?.openInventory(shop["main"])
    } else if (command.name.equals("sell", false)) {
      if (args.size != 1) {
        sender.sendMessage("&cPlease provide type <all|hand>!".colors())
        return true
      }
      if (sender !is Player) return true
      if (args.component1() == "hand") {
        if (sender.hasPermission("sell.hand")) sell(sender, sender.itemInHand, true)
        else sender.sendMessage("&aYou can't do this!".colors())
      } else if (args.component1() == "all") {
        if (sender.hasPermission("sell.all")) sell(sender, sender.itemInHand, false)
        else sender.sendMessage("&aYou can't do this!".colors())
      } else sender.sendMessage("&aArgument not found".colors())
      
    }
    return true
  }
  
  private fun reloadAll() {
    reloadConfig()
    InventoryManager.config = config
    shopConfig = ShopConfig.reloadConfig()
    shop = hashMapOf()
    addInventories()
    addItems()
  }
  
  private fun setupEconomy(): Boolean {
    if (server.pluginManager.getPlugin("Vault") == null) {
      return false
    }
    val rsp = server.servicesManager.getRegistration(Economy::class.java) ?: return false
    econ = rsp.provider
    return econ != null
  }
  
  private fun sell(p: Player, itemStack: ItemStack?, notAll: Boolean = false) {
    if (itemStack == null || itemStack.type == Material.AIR) {
      p.sendMessage("&cItem not found!".colors())
      return
    }
    val pair = getItemPrice(itemStack)
    if (pair == null) {
      p.sendMessage("&cItem price not listed in configuration.".colors())
      return
    }
    val price = pair.second
    val stack = pair.first
    getItemPrice(itemStack)
    if (notAll && stack.type == itemStack.type && stack.durability == itemStack.durability) {
      val amount = itemStack.amount
      p.itemInHand = null
      Shop.econ?.depositPlayer(p, amount.toDouble() * price)
      p.sendMessage("&aYou have sold $amount ${stack.type} for ${price * amount}".colors())
      return
    } else if (stack.type == itemStack.type && stack.durability == itemStack.durability) {
      val amount = getAllInventoryContains(p.inventory, stack)
      removeFromInventory(p.inventory, stack, amount)
      Shop.econ?.depositPlayer(p, amount.toDouble() * price)
      p.sendMessage("&aYou have sold $amount ${stack.type} for ${price * amount}".colors())
      return
    }
    p.sendMessage("&cItem price not listed in configuration.".colors())
  }
  
  internal fun getItemPrice(itemStack: ItemStack): Pair<ItemStack, Float>? {
    if (itemStack.type == Material.AIR) return null
    shopConfig.getKeys(false).forEach { store ->
      shopConfig.getConfigurationSection(store).getKeys(false).forEach { type ->
        val prefix = "$store.$type"
        val stack = itemBuilder {
          item = Material.valueOf(shopConfig.string("$prefix.material"))
          durability = if (Shop.shopConfig.contains("$prefix.durability"))
            Shop.shopConfig.getInt("$prefix.durability")
          else 0
        }
        if (stack.type == itemStack.type && stack.durability == itemStack.durability) {
          if (shopConfig.getString("$prefix.sell").toFloat() != 0f)
            return Pair(stack, shopConfig.getString("$prefix.sell").toFloat())

        }
      }
      
    }
    return null
  }
}

data class Purchase(
  val player: Player,
  val type: PurchaseType,
  var transaction: Transaction = Transaction.FAIL,
  val inventory: Inventory,
  val price: Float,
  val material: Material,
  val durability
  : Number
)

enum class Transaction {
  SUCCESS, FAIL
}

enum class PurchaseType {
  BUY, SELL;
  
  companion object {
    fun fromString(anotherString: String): PurchaseType {
      return valueOf(anotherString.toUpperCase())
    }
  }
}
