package com.chaoticmc.shop

import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

private lateinit var shopFile: File
private lateinit var shopConfig: YamlConfiguration

internal object ShopConfig {
  fun makeFiles(): YamlConfiguration {
    var new = false
    val instance = Shop.instance
    if (!instance.dataFolder.exists()) instance.dataFolder.mkdir()
    shopFile = File(instance.dataFolder, "shop.yml")
    with(shopFile) {
      if (!exists()) {
        createNewFile()
        new = true
      }
    }
    shopConfig = YamlConfiguration.loadConfiguration(shopFile)
    if (new) saveDefaultConfig()
    save()
    return shopConfig
  }
  
  private fun saveDefaultConfig() {
    listOf("blocks", "redstone", "plants", "tools", "dyes", "misc", "ores", "mobdrops").forEach {
      if (it != "main") {
        shopConfig.set("$it.1.material", "DIRT")
        shopConfig.set("$it.1.buy", "10.3")
        shopConfig.set("$it.1.sell", "2")
        println(it)
      }
    }
  }
  
  fun getConfig() = shopConfig
  
  fun reloadConfig(): YamlConfiguration {
    var new = false
    val instance = Shop.instance
    if (!instance.dataFolder.exists()) instance.dataFolder.mkdir()
    shopFile = File(instance.dataFolder, "shop.yml")
    with(shopFile) {
      if (!exists()) {
        createNewFile()
        new = true
      }
    }
    shopConfig = YamlConfiguration.loadConfiguration(shopFile)
    if (new) saveDefaultConfig()
    save()
    return shopConfig
  }
  
  fun save() = shopConfig.save(shopFile)
}