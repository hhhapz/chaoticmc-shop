package com.chaoticmc.shop.api.event

import com.chaoticmc.shop.PurchaseType
import com.chaoticmc.shop.Transaction
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import org.bukkit.inventory.Inventory

/**
 * @author hhhapz
 * @param[player] The player involved in the transaction
 * @param[type] Whether the [player] is buying or selling an Item
 * @param[transaction] either [Transaction.SUCCESS], or [Transaction.SUCCESS]. If nothing happens, returns [Transaction.FAIL] by default.
 * @param[inventory] returns the inventory full of the [material] being bought in transaction
 * @param[price] returns the price of the single [material] bought
 * @param[material] returns the involved material in the transaction
 * @param[amount] returns the amount of [material] in the transaction
 * @param[durability] normally 0, but can be different if selling some item with id, e.g. Colored Wool
 */
class PurchaseEvent(
  val player: Player,
  val type: PurchaseType,
  var transaction: Transaction = Transaction.FAIL,
  val inventory: Inventory,
  val price: Float,
  val material: Material,
  val amount: Int,
  val durability: Number
) : Event() {
  
  private val handlers = HandlerList()
  
  override fun getHandlers(): HandlerList = handlers
  
  fun getHandlerList(): HandlerList = handlers
  
}