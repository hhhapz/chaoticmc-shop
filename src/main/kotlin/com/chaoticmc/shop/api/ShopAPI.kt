package com.chaoticmc.shop.api

import com.chaoticmc.shop.Shop
import com.chaoticmc.shop.removeFromInventory
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

object ShopAPI {
  @JvmStatic
  fun getInstance() = Shop.instance
  
  @JvmStatic
  fun getShops() = Shop.shop

  @JvmStatic
    /**
     * @param[item] is the parameter of which you want to get a single item's price
     * @return a pair of the item with no item meta, and amount of one. Returns null if price doesn't exist
     * @author hhhapz
     */
  fun getPriceOfItem(item: ItemStack) = getInstance().getItemPrice(item)
  
  @JvmStatic
  fun removeItemsFromInventory(inv: Inventory, item: ItemStack){
    removeFromInventory(inv, item)
  }
}
