package com.chaoticmc.shop

import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryCloseEvent

internal class ClickListener : Listener {
  private val purchases = mutableListOf<Purchase>()
  @EventHandler
  fun onClick(e: InventoryClickEvent) {
    val config = Shop.instance.config
    with(e) {
      if (currentItem == null || currentItem == Material.AIR) return
      if (!currentItem.hasItemMeta()) return
      isCancelled = true
      val item = currentItem!!
      if (purchases.any { it.player == whoClicked }) {
        val purchase = purchases.find { it.player == whoClicked } ?: return
        if (purchase.material != currentItem.type || purchase.durability.toInt() != currentItem.durability.toInt()) {
          if (currentItem.type == Material.ARROW && currentItem.itemMeta.displayName == "&cGo back".colors())
            whoClicked.openInventory(Shop.shop["main"])
          return
        }
        InventoryManager.buyOrSell(purchase, currentItem.amount)
        return
      }
      
      if (!currentItem.itemMeta.hasLore()) return
      if (!Shop.shop.values.any { it == inventory }) return
      
      if (Shop.shop.values.find { it == inventory } == Shop.shop["main"]) {
        var shopName = config.getConfigurationSection("settings").getKeys(false).find {
          item.itemMeta.displayName == config.string("settings.$it.title")
        } ?: return
        whoClicked.openInventory(Shop.shop[shopName])
      }
      
      val price = Shop.instance.getItemPrice(item)?.second ?: return
      val inv = makeInventory(27, "&e&lChoose Amount".colors())
      var choice: String
      val lore = item.itemMeta.lore
      if (isLeftClick && lore.any { it.contains("Left-Click") && it.contains("to buy") })
        choice = "Buy"
      else if (isRightClick && lore.any { it.contains("Right-Click") && it.contains("to sell") })
        choice = "Sell"
      else return
      
      whoClicked.openInventory(inv)
      purchases.add(Purchase(
        whoClicked as Player,
        PurchaseType.fromString(choice),
        Transaction.FAIL,
        inv,
        price,
        item.type,
        item.durability))
      
      InventoryManager.makeInventory(inv, item, choice, price)
      
      return
    }
  }
  
  @EventHandler
  fun onClose(e: InventoryCloseEvent) {
    if (purchases.any { e.player == it.player }) {
      purchases.remove(purchases.find { e.player == it.player }!!)
    }
  }
}



