package com.chaoticmc.shop

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

internal fun FileConfiguration.string(path: String, changeColor: Boolean = true): String {
  var string = getString(path)
  if (changeColor) string = string.colors()
  return string
}

internal fun String?.colors(prefix: Char = '&'): String {
  if (this == null) return ""
  return ChatColor.translateAlternateColorCodes(prefix, this)!!
}

internal fun makeInventory(size: Int, name: String = "") = Bukkit.createInventory(null, size, name)!!

internal fun makeBlock(item: Material,
                       size: Int = 1,
                       durability: Int = 0,
                       dispName: String = "",
                       lore: MutableList<String> = mutableListOf()): ItemStack {
  val stack = ItemStack(item, size, durability.toShort())
  val meta = stack.itemMeta
  meta.displayName = dispName.colors()
  val tempLore = mutableListOf<String>()
  lore.forEach { tempLore.add(it.colors()) }
  meta.lore = tempLore
  stack.itemMeta = meta
  return stack
}

internal fun itemBuilder(builder: DSLItemBuilder.() -> Unit): ItemStack {
  val itemBuilder = DSLItemBuilder()
  itemBuilder.builder()
  return itemBuilder.build()
}

data class DSLItemBuilder(var item: Material = Material.AIR,
                          var amount: Int = 1,
                          var durability: Int = 0,
                          var dispName: String = "",
                          var lore: MutableList<String> = mutableListOf()) {
  internal fun build() = makeBlock(item, amount, durability, dispName, lore)
}

internal fun inventoryContains(inventory: Inventory, item: ItemStack): Boolean {
  var count = 0
  val items = inventory.contents
  for (i in items.indices) {
    if (items[i] != null && items[i].type == item.type && items[i].durability == item.durability)
      count += items[i].amount
    if (count >= item.amount) {
      return true
    }
  }
  return false
}

internal fun getAllInventoryContains(inventory: Inventory, item: ItemStack): Int {
  var count = 0
  val items = inventory.contents
  for (i in items.indices)
    if (items[i] != null && items[i].type == item.type && items[i].durability == item.durability)
      count += items[i].amount
  return count
  
}

internal fun removeFromInventory(inventory: Inventory, item: ItemStack, amnt: Int = item.amount) {
  var amt = amnt
  val items = inventory.contents
  for (i in items.indices) {
    if (items[i] != null && items[i].type == item.type && items[i].durability == item.durability) {
      if (items[i].amount > amt) {
        items[i].amount = items[i].amount - amt
        break
      } else if (items[i].amount == amt) {
        items[i] = null
        break
      } else {
        amt -= items[i].amount
        items[i] = null
      }
    }
  }
  inventory.contents = items
}